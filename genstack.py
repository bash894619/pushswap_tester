import random
import sys

# usage: python genstack.py stacksize min max
# exemple: python genstack.py 10 0 1000
# will create 10 unique numbers from 0 to 1000
# like: 138 527 947 967 811 33 112 526 949 27

if len(sys.argv) < 2:
    sys.exit("Usage: python genstack.py stacksize [min] [max]")

stacksize = int(sys.argv[1])
if stacksize < 0:
    sys.exit("Error: stacksize can't be negative")

if len(sys.argv) > 2:
    min_val = int(sys.argv[2])
else:
    min_val = 0

if len(sys.argv) > 3:
    max_val = int(sys.argv[3])
    if stacksize >= max_val - min_val + 1:
        sys.exit("Error: range too small")
else:
    max_val = min_val + stacksize

stack = []
while len(stack) < stacksize:
    n = random.randint(min_val, max_val)
    if n not in stack:
        stack.append(n)

print(" ".join(str(n) for n in stack))
