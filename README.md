# A tester for Push_swap@42 - To see what's going on
This tester shows the performance of your push_swap program.


## How do I run this tester?
First of all, place yourself in your project's folder. Then clone this repository inside of it. You can then run this command:

```bash
cd pushswap_tester &&
bash tester.sh [path-to-push-swap-dir] [stack-size 0R range] [nb_of_tests]
```

***
#### for example:
the following command will perform 100 testss with a stack of 100 integers
```bash
bash tester.sh ../ 100 100
```

***
## Push_swap source

Link to my push_swap:
https://gitlab.com/annexes/pushswap
